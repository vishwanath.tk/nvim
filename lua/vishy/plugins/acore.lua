return {
  -- Used by various other plugins
  { 'nvim-lua/plenary.nvim' },

  -- Colorscheme
  {
    'folke/tokyonight.nvim',
    lazy = false,
    priority = 1000,
    opts = {
      style = 'storm',
      transparent = true,
      styles = {
        sidebars = 'transparent',
        floats = 'transparent',
      },
    },
    config = function(_, opts)
      local tokyonight = require('tokyonight')
      tokyonight.setup(opts)
      tokyonight.load()
    end,
  },

  -- Startuptime
  { 'dstein64/vim-startuptime' },

  print('Plugins Core'),
}
