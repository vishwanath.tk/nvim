local M = {
  'nvim-neo-tree/neo-tree.nvim',
  cmd = 'Neotree',
  branch = 'v2.x',
  dependencies = {
    'nvim-lua/plenary.nvim',
    'nvim-tree/nvim-web-devicons',
    'MunifTanjim/nui.nvim',
  },
  event = 'BufRead',
  keys = {
    { '<C-\\>', '<cmd>Neotree toggle<cr>', desc = 'NeoTree' },
  },
}

function M.config()
  require('neo-tree').setup({
    popup_border_style = 'rounded',
    window = {
      position = 'right',
      width = 40,
      mapping_options = {
        noremap = true,
        nowait = true,
      },
    },
    filesystem = {
      follow_current_file = true,
      hijack_netrw_behavior = 'open_current',
    },
  })
end

vim.api.nvim_create_autocmd({ 'FileType' }, {
  pattern = { 'neo-tree' },
  callback = function()
    vim.opt_local.number = false
    setlocal nonumber norelativenumber
  end,
})

return M
