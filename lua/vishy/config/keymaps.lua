local map = require('vishy.utils').map

-- leader mappings
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '
map('', '<Space>', '<Nop>')

-- basic mappings
map('', '<C-s>', ':w<CR>')
map('', '<C-c>b', ':bd<CR>')
map('', '<F3>', ':nohlsearch<CR>')
map('', '<C-f>', '<C-f>zz')
map('', '<C-b>', '<C-b>zz')

-- move line(s) up/down
map('v', '<A-Up>', ':m \'<-2<CR>gv=gv')
map('v', '<A-Down>', ':m \'>+1<CR>gv=gv')

-- laststatus mappings
map('n', '<S-s>', [[:set laststatus=2<CR>]])
map('n', '<S-h>', [[:set laststatus=0<CR>]])

print('Keymaps')
