local scopes = { o = vim.o, b = vim.bo, w = vim.wo }
local g = vim.g

local function opt(scope, key, value)
  scopes[scope][key] = value
  if scope ~= 'o' then
    scopes['o'][key] = value
  end
end

local indent, width = 4, 120

-- [[ Context ]]
opt('o', 'termguicolors', true) -- bool: Enable truecolor support
opt('w', 'number', true) -- bool: Show line numbers
opt('w', 'relativenumber', true) -- bool: Show relative line numbers (default)
opt('w', 'signcolumn', 'yes') -- num: Show sign column
opt('o', 'laststatus', 0) -- num: Start with statusline disabled
opt('o', 'updatetime', 250) -- num: Delay before swap file is saved
opt('w', 'statuscolumn', '%=%{v:relnum?v:relnum:v:lnum} ') -- str: Single column for relative/number
-- opt('w', 'colorcolumn', tostring(width)) -- str: Line length marker (right column)
opt('b', 'textwidth', width) -- num: Maximum width of text
opt('o', 'showmode', false) -- bool: Show current mode on status line
opt('o', 'mouse', nil) -- nil: Disable mouse

-- [[ Tab/Space/Indent ]]
opt('b', 'expandtab', true) -- bool: Use spaces instead of <Tab> characters
opt('b', 'tabstop', indent) -- num: Set <Tab> size
opt('b', 'softtabstop', indent) -- num: Number of spaces a <Tab> counts for
opt('b', 'shiftwidth', indent) -- num: Size of (auto)indentation
opt('o', 'shiftround', true) -- bool: Round indent

-- [[ Search ]]
opt('o', 'ignorecase', true) -- bool: Ignore case in search patterns
opt('o', 'smartcase', true) -- bool: Don't 'ignorecase' when pattern has uppercase

-- [[ Scrolling ]]
opt('o', 'scrolloff', 4) -- num: Lines of context
opt('o', 'sidescrolloff', 8) -- num: Columns of context

-- [[ Window Splits ]]
opt('o', 'splitbelow', true) -- bool: Put new windows below current
opt('o', 'splitright', true) -- bool: Put new windows right of current

-- [[ Formatting/Completion ]]
opt('b', 'formatoptions', 'tcrqnj') -- str: Automatic formatting options
opt('o', 'completeopt', 'menuone,preview,noinsert,noselect') -- str: Completion options
opt('o', 'wildmode', 'list:longest') -- str: Command-line completion mode

-- [[ Providers ]]
g.loaded_python_provider = 0 -- num: Disable pthon provider
g.loaded_python3_provider = 0 -- num: Disable python3 provider
g.loaded_node_provider = 0 -- num: Disable node provider
g.loaded_perl_provider = 0 -- num: Disable perl provider
g.loaded_ruby_provider = 0 -- num: Disable ruby provider

-- Highlight on yank
vim.cmd([[
  augroup YankHighlight
    autocmd!
    autocmd TextYankPost * silent! lua vim.highlight.on_yank()
  augroup end
]])

print('Options')
