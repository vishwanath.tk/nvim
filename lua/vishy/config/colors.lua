-- [[ Colorscheme ]]
function SetColorScheme(color)
  local tokyonight_path = vim.fn.stdpath('data') .. '/lazy/tokyonight.nvim'
  -- print(tokyonight_path)
  local color = 'default'

  if vim.fn.isdirectory(tokyonight_path) == 1 then
    color = 'tokyonight'

    vim.g.tokyonight_colors = { border = '#292e42' }
    vim.g.tokyonight_style = 'storm'
  else
    color = color
  end

  vim.cmd.colorscheme(color)

  -- vim.api.nvim_set_hl(0, 'Normal', { ctermbg='none', bg='none' })
  -- vim.api.nvim_set_hl(0, 'NormalNC', { ctermbg='none', bg='none' })
  -- vim.api.nvim_set_hl(0, 'NormalFloat', { ctermbg='none', bg='none' })
  -- vim.api.nvim_set_hl(0, 'TelescopeBorder', { ctermbg='none', bg='none' })
end

function enable_transparent_mode()
  vim.api.nvim_create_autocmd('ColorScheme', {
    pattern = '*',
    callback = function()
      local hl_groups = {
        'Normal',
        'SignColumn',
        'NormalNC',
        'NvimTreeNormal',
        'EndOfBuffer',
        'MsgArea',
      }
      for _, name in ipairs(hl_groups) do
        vim.cmd(string.format('highlight %s ctermbg=none guibg=none', name))
      end
    end,
  })
  vim.opt.fillchars = 'eob: '
end

function telescope_transparent()
  vim.api.nvim_create_autocmd('ColorScheme', {
    pattern = '*',
    callback = function()
      local hl_groups = {
        'TelescopeNormal',
        'TelescopeBorder',
        'TelescopePromptBorder',
        'TelescopePromptNormal',
        'TelescopePromptPrefix',
        'TelescopePreviewTitle',
        'TelescopePromptTitle',
        'TelescopeResultsTitle',
        'TelescopeSelection',
        'TelescopeResultsDiffAdd',
        'TelescopeResultsDiffChange',
        'TelescopeResultsDiffDelete',
      }
      for _, name in ipairs(hl_groups) do
        vim.cmd(string.format('highlight %s ctermbg=none guibg=none', name))
      end
    end,
  })
  vim.opt.fillchars = 'eob: '
end

SetColorScheme()
enable_transparent_mode()
telescope_transparent()

print('Colors')
